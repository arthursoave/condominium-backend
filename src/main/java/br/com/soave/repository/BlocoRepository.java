package br.com.soave.repository;

import br.com.soave.model.Bloco;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BlocoRepository extends CrudRepository<Bloco, Long> {

    List<Bloco> findBlocoByCodigoBloco(Long codigoBloco);
    List<Bloco> findBlocoByCodigoCondominio(Long codigoCondominio);

}
