package br.com.soave.repository;

import br.com.soave.model.Entrega;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EntregaRepository extends CrudRepository<Entrega, Long> {

    Entrega findEntregaByCodigoEntrega(Long codigoEntrega);

}
