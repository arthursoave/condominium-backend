package br.com.soave.repository;

import br.com.soave.model.Apartamento;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ApartamentoRepository extends CrudRepository<Apartamento, Long> {

    List<Apartamento> findByCodigoApartamento(Long codigoApartamento);

}
