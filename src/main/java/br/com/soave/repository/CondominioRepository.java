package br.com.soave.repository;

import br.com.soave.model.Condominio;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CondominioRepository extends CrudRepository<Condominio, Long > {

    Condominio findCondominioByCodigoCondominio(Long codigoCondominio);
    Condominio findCondominioByNmeFantasiaCondominioContains(String nomeCondominio);
    List<Condominio> findAll();

}
