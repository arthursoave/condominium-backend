package br.com.soave.service;

import br.com.soave.model.Bloco;

import java.util.List;

public interface BlocoService {
    void incluirBloco(Bloco bloco);
    void atualizarBloco(Bloco bloco);
    void excluirBloco(Long codigoBloco);
    List<Bloco> listarBlocosDoCondominio(Long codigoCondominio);
    List<Bloco> findByCodigoBloco(Long codigoBloco);
}
