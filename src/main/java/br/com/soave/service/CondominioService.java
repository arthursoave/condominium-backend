package br.com.soave.service;

import br.com.soave.model.Condominio;

import java.util.List;

public interface CondominioService {
    List<Condominio> listarCondominios();
    Condominio findByCodigoCondominio(Long idCondominio);
    Condominio findByName(String nomeCondominio);
    void incluirCondominio(Condominio condominio);
    void excluirCondominio(Long idCondominio);
    void atualizarCondominio(Condominio condominio);

}
