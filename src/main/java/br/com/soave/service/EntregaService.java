package br.com.soave.service;

import br.com.soave.model.Entrega;

import java.util.List;

public interface EntregaService {
    List<Entrega> obterEntregas();
    Entrega obterEntregaPorCodigo(Long idEntrega);
    Entrega obterEntregaPorApartamento(Long idEntrega, Long idApartamento);
    void atualizarEntrega(Entrega entrega);
    void excluirEntrega(Long idEntrega);
    void incluirEntrega(Entrega entrega);
}
