package br.com.soave.service.impl;

import br.com.soave.model.Bloco;
import br.com.soave.repository.BlocoRepository;
import br.com.soave.service.BlocoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlocoServiceImpl implements BlocoService {

    @Autowired
    private BlocoRepository blocoRepository;

    @Override
    public void incluirBloco(Bloco bloco) {
        blocoRepository.save(bloco);
    }

    @Override
    public void atualizarBloco(Bloco bloco) {
        blocoRepository.save(bloco);
    }

    @Override
    public void excluirBloco(Long codigoBloco) {
        blocoRepository.deleteById(codigoBloco);
    }

    @Override
    public List<Bloco> listarBlocosDoCondominio(Long codigoCondominio) {
        return blocoRepository.findBlocoByCodigoCondominio(codigoCondominio);
    }

    @Override
    public List<Bloco> findByCodigoBloco(Long codigoBloco) {
        return blocoRepository.findBlocoByCodigoBloco(codigoBloco);
    }
}
