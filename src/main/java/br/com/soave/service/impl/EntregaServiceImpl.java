package br.com.soave.service.impl;

import br.com.soave.model.Entrega;
import br.com.soave.repository.EntregaRepository;
import br.com.soave.service.EntregaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EntregaServiceImpl implements EntregaService {

    @Autowired
    private EntregaRepository entregaRepository;

    @Override
    public List<Entrega> obterEntregas() {
        return null;//entregaRepository.findAll();
    }

    @Override
    public Entrega obterEntregaPorCodigo(Long idEntrega) {
        return entregaRepository.findEntregaByCodigoEntrega(idEntrega);
    }

    @Override
    public Entrega obterEntregaPorApartamento(Long idEntrega, Long idApartamento) {
        return null;
    }

    @Override
    public void atualizarEntrega(Entrega entrega) {
        entregaRepository.save(entrega);
    }

    @Override
    public void excluirEntrega(Long idEntrega) {
        entregaRepository.deleteById(idEntrega);
    }

    @Override
    public void incluirEntrega(Entrega entrega) {
        entregaRepository.save(entrega);
    }
}
