package br.com.soave.service.impl;

import br.com.soave.model.Condominio;
import br.com.soave.repository.CondominioRepository;
import br.com.soave.service.CondominioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CondominioServiceImpl implements CondominioService {

    @Autowired
    private CondominioRepository condominioRepository;

    @Override
    public List<Condominio> listarCondominios() {
        return condominioRepository.findAll();
    }

    @Override
    public Condominio findByCodigoCondominio(Long idCondominio) {
        return condominioRepository.findCondominioByCodigoCondominio(idCondominio);
    }

    @Override
    public Condominio findByName(String nomeCondominio) {
        return condominioRepository.findCondominioByNmeFantasiaCondominioContains(nomeCondominio);
    }

    @Override
    public void incluirCondominio(Condominio condominio) {
        condominioRepository.save(condominio);
    }

    @Override
    public void excluirCondominio(Long idCondominio) {
        condominioRepository.deleteById(idCondominio);
    }

    @Override
    public void atualizarCondominio(Condominio condominio) {
        condominioRepository.save(condominio);
    }
}
