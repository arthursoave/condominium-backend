package br.com.soave.api;

import br.com.soave.model.Condominio;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api("Controller for 'Condominiuns' operations")
public interface CondominioController {

    @ApiOperation(value = "Incluir novo Condominio")
    @RequestMapping(method = RequestMethod.POST, path = "/incluir-condominio")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Condominio incluida com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void incluirCondominio(Condominio condominio);
    //TODO: update some condominium
    @ApiOperation(value = "Alterar um Condominio")
    @RequestMapping(method = RequestMethod.PUT, path = "/alterar-condominio")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Condominio alterado com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void alterarCondominio(Condominio condominio);
    //TODO: delete some condominium
    @ApiOperation(value = "Excluir Condominio")
    @RequestMapping(method = RequestMethod.DELETE, path = "/deletar-condominio")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Condominio removido com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void excluirCondominio(Long codigoCondominio);
    //TODO: find condominium by id
    //Search Methods
    //Search by Category code
    @ApiOperation(value = "Buscar Condominio pelo codigo")
    @RequestMapping(method = RequestMethod.GET, path = "/buscar-condominio-codigo", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Condominio encontrado com sucesso"),
            @ApiResponse(code = 401, message = "Operacaoo nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    Condominio buscarCondominioPorCodigo(Long codigoCondominio);
    //TODO: find condominium by name
    //Search by name (since the user input 3 letters
    @ApiOperation(value = "Buscar Condominio pelo nome")
    @RequestMapping(method = RequestMethod.GET, path = "/buscar-condominio-nome", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Condominio encontrado com sucesso"),
            @ApiResponse(code = 401, message = "Operacaoo nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    Condominio buscarCondominioPorNomeNumero(String nomeNumeroCondominio);

    @ApiOperation(value = "Listar todos os condominios")
    @RequestMapping(method = RequestMethod.GET, path = "/listar-condominios", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Condominios encontrados com sucesso"),
            @ApiResponse(code = 401, message = "Operacaoo nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    List<Condominio> listarCondominios();
}