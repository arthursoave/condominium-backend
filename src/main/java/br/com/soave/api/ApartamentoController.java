package br.com.soave.api;

import br.com.soave.model.Apartamento;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api("Controller for 'Apartment' operations")
public interface ApartamentoController {
    //TODO: include new apartment
    @ApiOperation(value = "Incluir novo Apartamento")
    @RequestMapping(method = RequestMethod.POST, path = "/incluir-apartamento")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Apartamento incluida com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void incluirApartamento(Apartamento apartamento);
    //TODO: update some apartment
    @ApiOperation(value = "Alterar um Apartamento")
    @RequestMapping(method = RequestMethod.PUT, path = "/alterar-apartamento")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Apartamento alterado com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void alterarApartamento(Apartamento apartamento);
    //TODO: delete some apartment
    @ApiOperation(value = "Excluir novo Apartamento")
    @RequestMapping(method = RequestMethod.DELETE, path = "/deletar-apartamento")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Apartamento removido com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void excluirApartamento(Long codigoApartamento);
    //TODO: search apartment by id
    //Search Methods
    //Search by Category code
    @ApiOperation(value = "Buscar Apartamento pelo codigo")
    @RequestMapping(method = RequestMethod.GET, path = "/buscar-apartamento-codigo", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Apartamento encontrado com sucesso"),
            @ApiResponse(code = 401, message = "Operacaoo nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    Apartamento buscarApartamentoPorCodigo(Long codigoApartamento);
    //TODO: search apartment by name or number
    //Search by name (since the user input 3 letters
    @ApiOperation(value = "Buscar Apartamento pelo nome ou numero")
    @RequestMapping(method = RequestMethod.GET, path = "/buscar-apartamento-nome-numero", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Apartamento encontrado com sucesso"),
            @ApiResponse(code = 401, message = "Operacaoo nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    Apartamento buscarApartamentoPorNomeNumero(String nomeNumeroApartamento);
}
