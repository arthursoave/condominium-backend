package br.com.soave.api;

import br.com.soave.model.Entrega;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Api("Controller for 'deliveries' operations")
public interface EntregaController {
    //todo: include new delivery
    @ApiOperation(value = "Incluir novo entrega")
    @RequestMapping(method = RequestMethod.POST, path = "/incluir-entrega")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "entrega incluida com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void incluirEntrega(Entrega entrega);
    //todo: update some delivery
    @ApiOperation(value = "Alterar um entrega")
    @RequestMapping(method = RequestMethod.PUT, path = "/alterar-entrega")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "entrega alterado com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void alterarEntrega(Entrega entrega);
    //todo: delete some delivery
    @ApiOperation(value = "Excluir um entrega")
    @RequestMapping(method = RequestMethod.DELETE, path = "/deletar-entrega")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "entrega removido com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void excluirEntrega(Long codigoentrega);
    //todo: find delivery by id
    //Search Methods
    //Search by Category code
    @ApiOperation(value = "Buscar entrega pelo codigo")
    @RequestMapping(method = RequestMethod.GET, path = "/buscar-entrega-codigo", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "entrega encontrado com sucesso"),
            @ApiResponse(code = 401, message = "Operacaoo nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    Entrega buscarEntregaPorCodigo(Long codigoentrega);
    //todo: find delivery by apartment
    //Search by name (since the user input 3 letters
    @ApiOperation(value = "Buscar entrega pelo nome")
    @RequestMapping(method = RequestMethod.GET, path = "/buscar-entrega-nome-numero", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "entrega encontrado com sucesso"),
            @ApiResponse(code = 401, message = "Operacaoo nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    Entrega buscarEntregaPorNomeNumero(String nomeNumeroentrega);
}

