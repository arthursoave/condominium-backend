package br.com.soave.api;

import br.com.soave.model.Bloco;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api("Controller for 'bloco' operations")
public interface BlocoController {
    //TODO: include new apartment block
    @ApiOperation(value = "Incluir novo bloco")
    @RequestMapping(method = RequestMethod.POST, path = "/incluir-bloco")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "bloco incluida com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void incluirbloco(Bloco bloco);
    //todo: update some apartment block
    @ApiOperation(value = "Alterar um bloco")
    @RequestMapping(method = RequestMethod.PUT, path = "/alterar-bloco")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "bloco alterado com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void alterarbloco(Bloco bloco);
    //todo: delete some apartment block
    @ApiOperation(value = "Excluir um bloco")
    @RequestMapping(method = RequestMethod.DELETE, path = "/deletar-bloco")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "bloco removido com sucesso"),
            @ApiResponse(code = 401, message = "Operacao nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    void excluirbloco(Long codigobloco);
    //todo: find some apartment block by id
    //Search Methods
    //Search by Category code
    @ApiOperation(value = "Buscar Bloco pelo codigo")
    @RequestMapping(method = RequestMethod.GET, path = "/buscar-bloco-codigo", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "bloco encontrado com sucesso"),
            @ApiResponse(code = 401, message = "Operacaoo nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    List<Bloco> buscarblocoPorCodigo(Long codigobloco);
    //todo: find some apartment block by condominium name
    //Search by name (since the user input 3 letters
    @ApiOperation(value = "Buscar Bloco pelo codigo do condominio")
    @RequestMapping(method = RequestMethod.GET, path = "/buscar-bloco-codigo-condominio", produces = "application/json")
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "bloco encontrado com sucesso"),
            @ApiResponse(code = 401, message = "Operacaoo nao autorizada"),
            @ApiResponse(code = 403, message = "Voce nao possui permissao para esta operacao"),
            @ApiResponse(code = 500, message = "Falha de comunicacao com o servidor da aplicacao")})
    List<Bloco> buscarblocoPorCodigoCondominio(Long codigoCondominio);

}
