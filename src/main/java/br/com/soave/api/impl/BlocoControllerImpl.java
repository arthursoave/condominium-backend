package br.com.soave.api.impl;

import br.com.soave.api.BlocoController;
import br.com.soave.model.Bloco;
import br.com.soave.service.BlocoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BlocoControllerImpl implements BlocoController {

    @Autowired
    private BlocoService blocoService;

    @Override
    public void incluirbloco(@RequestBody Bloco bloco) {
        blocoService.incluirBloco(bloco);
    }

    @Override
    public void alterarbloco(@RequestBody Bloco bloco) {
        blocoService.atualizarBloco(bloco);
    }

    @Override
    public void excluirbloco(@RequestParam Long codigobloco) {
        blocoService.excluirBloco(codigobloco);
    }

    @Override
    public List<Bloco> buscarblocoPorCodigo(Long codigobloco) {
        return blocoService.findByCodigoBloco(codigobloco);
    }

    @Override
    public List<Bloco> buscarblocoPorCodigoCondominio(Long codigoCondominio) {
        return blocoService.listarBlocosDoCondominio(codigoCondominio);
    }

}
