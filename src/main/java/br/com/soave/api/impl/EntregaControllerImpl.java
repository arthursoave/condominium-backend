package br.com.soave.api.impl;

import br.com.soave.api.EntregaController;
import br.com.soave.model.Apartamento;
import br.com.soave.model.Bloco;
import br.com.soave.model.Entrega;
import br.com.soave.service.EntregaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("entrega")
public class EntregaControllerImpl implements EntregaController {

    @Autowired
    EntregaService entregaService;

    @Override
    public void incluirEntrega(@RequestBody Entrega entrega) {
        entregaService.incluirEntrega(entrega);
    }

    @Override
    public void alterarEntrega(@RequestBody Entrega entrega) {
        entregaService.atualizarEntrega(entrega);
    }

    @Override
    public void excluirEntrega(@RequestParam Long codigoentrega) {
        entregaService.excluirEntrega(codigoentrega);
    }

    @Override
    public Entrega buscarEntregaPorCodigo(@RequestParam Long codigoentrega) {
        return entregaService.obterEntregaPorCodigo(codigoentrega);
    }

    @Override
    public Entrega buscarEntregaPorNomeNumero(@RequestParam String nomeNumeroentrega) {
        return new Entrega();
    }
}
