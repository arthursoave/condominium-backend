package br.com.soave.api.impl;

import br.com.soave.api.ApartamentoController;
import br.com.soave.model.Apartamento;
import br.com.soave.model.Bloco;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApartamentoControllerImpl implements ApartamentoController {


    @Override
    public void incluirApartamento(@RequestBody Apartamento apartamento) {

    }

    @Override
    public void alterarApartamento(Apartamento apartamento) {

    }

    @Override
    public void excluirApartamento(Long codigoApartamento) {

    }

    @Override
    public Apartamento buscarApartamentoPorCodigo(@RequestParam Long codigoApartamento) {
        //TODO: Remove mock data
        Apartamento ap = new Apartamento();
        Bloco bl = new Bloco();
        ap.setCodigoApartamento(1L);
        ap.setEmailContato("atrpaula@gmail.com");
        ap.setNumeroApartamento(23);
        ap.setTelefoneContato(119870199999L);

        bl.setCodigoBloco(19L);
        ap.setBloco(bl);
        return ap;
    }

    @Override
    public Apartamento buscarApartamentoPorNomeNumero(@RequestParam String nomeNumeroApartamento) {
        return new Apartamento();
    }
}
