package br.com.soave.api.impl;

import br.com.soave.api.CondominioController;
import br.com.soave.model.Condominio;
import br.com.soave.service.CondominioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("condominio")
public class CondominioControllerImpl implements CondominioController {

    @Autowired
    private CondominioService condominioService;

    @Override
    public void incluirCondominio(@RequestBody Condominio condominio) {
        condominioService.incluirCondominio(condominio);
    }

    @Override
    public void alterarCondominio(@RequestBody Condominio condominio) {
        condominioService.atualizarCondominio(condominio);
    }

    @Override
    public void excluirCondominio(@RequestParam Long codigoCondominio) {
        condominioService.excluirCondominio(codigoCondominio);
    }

    @Override
    public Condominio buscarCondominioPorCodigo(@RequestParam Long codigoCondominio) {
        return condominioService.findByCodigoCondominio(codigoCondominio);
    }

    @Override
    public Condominio buscarCondominioPorNomeNumero(@RequestParam String nomeNumeroCondominio) {
        return condominioService.findByName(nomeNumeroCondominio);
    }

    @Override
    public List<Condominio> listarCondominios(){
        return condominioService.listarCondominios();
    }
}
