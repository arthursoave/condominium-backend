package br.com.soave.model;


import com.fasterxml.jackson.annotation.JsonRootName;
import org.hibernate.annotations.Columns;

import javax.persistence.*;

/**
 * Model/dataBase for a condominium
 *
 * @author soave
 * @since 10/2018
 */

@Entity
@JsonRootName(value = "condominio")
public class Condominio {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "condominium_Sequence")
    @SequenceGenerator(name = "condominium_Sequence", sequenceName = "COND_SEQ")
    @Column(name = "CODIGO_CONDOMINIO")
    private Long codigoCondominio;

    @Column(name = "RZ_SOCIAL_CONDOMINIO")
    private String razaoSocialCondominio;

    @Column(name = "NM_FANTASIA_CONDOMINIO")
    private String nmeFantasiaCondominio;

    @Column(name = "LOGRADOURO_CONDOMINIO")
    private String logradouroCondominio;

    @Column(name = "NUMERO_END_CONDOMINIO")
    private String numeroEndCondominio;

    @Column(name = "BAIRRO_CONDOMINIO")
    private String bairroCondominio;

    @Column(name = "CIDADE_CONDOMINIO")
    private String cidadeCondominio;

    @Column(name = "NME_RESPONSAVEL_CONDOMINIO")
    private String nmeResponsavelCondominio;

    @Column(name = "TELEFONE_CONDOMINIO")
    private Long telefoneCondominio;

    @Column(name = "TELEFONE_RESPONSAVEL")
    private Long telefoneResponsavel;

    @Column(name = "EMAIL_CONDOMINIO")
    private String emailCondominio;

    public Long getCodigoCondominio() {
        return codigoCondominio;
    }

    public void setCodigoCondominio(Long codigoCondominio) {
        this.codigoCondominio = codigoCondominio;
    }

    public String getRazaoSocialCondominio() {
        return razaoSocialCondominio;
    }

    public void setRazaoSocialCondominio(String razaoSocialCondominio) {
        this.razaoSocialCondominio = razaoSocialCondominio;
    }

    public String getNmeFantasiaCondominio() {
        return nmeFantasiaCondominio;
    }

    public void setNmeFantasiaCondominio(String nmeFantasiaCondominio) {
        this.nmeFantasiaCondominio = nmeFantasiaCondominio;
    }

    public String getLogradouroCondominio() {
        return logradouroCondominio;
    }

    public void setLogradouroCondominio(String logradouroCondominio) {
        this.logradouroCondominio = logradouroCondominio;
    }

    public String getNumeroEndCondominio() {
        return numeroEndCondominio;
    }

    public void setNumeroEndCondominio(String numeroEndCondominio) {
        this.numeroEndCondominio = numeroEndCondominio;
    }

    public String getBairroCondominio() {
        return bairroCondominio;
    }

    public void setBairroCondominio(String bairroCondominio) {
        this.bairroCondominio = bairroCondominio;
    }

    public String getCidadeCondominio() {
        return cidadeCondominio;
    }

    public void setCidadeCondominio(String cidadeCondominio) {
        this.cidadeCondominio = cidadeCondominio;
    }

    public String getNmeResponsavelCondominio() {
        return nmeResponsavelCondominio;
    }

    public void setNmeResponsavelCondominio(String nmeResponsavelCondominio) {
        this.nmeResponsavelCondominio = nmeResponsavelCondominio;
    }

    public Long getTelefoneCondominio() {
        return telefoneCondominio;
    }

    public void setTelefoneCondominio(Long telefoneCondominio) {
        this.telefoneCondominio = telefoneCondominio;
    }

    public Long getTelefoneResponsavel() {
        return telefoneResponsavel;
    }

    public void setTelefoneResponsavel(Long telefoneResponsavel) {
        this.telefoneResponsavel = telefoneResponsavel;
    }

    public String getEmailCondominio() {
        return emailCondominio;
    }

    public void setEmailCondominio(String emailCondominio) {
        this.emailCondominio = emailCondominio;
    }
}
