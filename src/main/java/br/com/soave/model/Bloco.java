package br.com.soave.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonRootName;

import javax.persistence.*;

/**
 * Model/dataBase for a group of apartments
 *
 * @author soave
 * @since 10/2018
 */

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonRootName(value = "bloco")
public class Bloco {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "bloco_Sequence")
    @SequenceGenerator(name = "bloco_Sequence", sequenceName = "BLOCO_SEQ")
    private Long codigoBloco;

    @Column(name = "LETRA_NUMERO_BLOCO")
    private String letraNumeroBloco;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO_CONDOMINIO", referencedColumnName = "CODIGO_CONDOMINIO" ,nullable = false)
    @JsonBackReference
    private Condominio codigoCondominio;

    public Long getCodigoBloco() {
        return codigoBloco;
    }

    public void setCodigoBloco(Long codigoBloco) {
        this.codigoBloco = codigoBloco;
    }

    public String getLetraNumeroBloco() {
        return letraNumeroBloco;
    }

    public void setLetraNumeroBloco(String letraNumeroBloco) {
        this.letraNumeroBloco = letraNumeroBloco;
    }

    public Condominio getCodigoCondominio() {
        return codigoCondominio;
    }

    public void setCodigoCondominio(Condominio codigoCondominio) {
        this.codigoCondominio = codigoCondominio;
    }
}
