package br.com.soave.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonRootName;

import javax.persistence.*;
import java.util.Date;

/**
 * Model/dataBase model for some delivery
 *
 * @author soave
 * @since 10/2018
 */

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonRootName(value = "entrega")
public class Entrega {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "entrega_Sequence")
    @SequenceGenerator(name = "entrega_Sequence", sequenceName = "ENTREGA_SEQ")
    private Long codigoEntrega;

    @Column(name = "TIPO_ENTREGA")
    private String tipoEntrega;

    @Column(name = "FLG_ALTO_VALOR")
    private String flgAltoValor;

    @Column(name = "FLG_MERCADORIA_FRAGIL")
    private String flgMercadoriaFragil;

    @Column(name = "DATA_ENTRADA")
    private Date dataEntrada;

    @Column(name = "DATA_RETIRADA")
    private Date dataRetirada;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO_APARTAMENTO", nullable = false)
    @JsonManagedReference
    private Apartamento apartamento;

    public Long getCodigoEntrega() {
        return codigoEntrega;
    }

    public void setCodigoEntrega(Long codigoEntrega) {
        this.codigoEntrega = codigoEntrega;
    }

    public String getTipoEntrega() {
        return tipoEntrega;
    }

    public void setTipoEntrega(String tipoEntrega) {
        this.tipoEntrega = tipoEntrega;
    }

    public String getFlgAltoValor() {
        return flgAltoValor;
    }

    public void setFlgAltoValor(String flgAltoValor) {
        this.flgAltoValor = flgAltoValor;
    }

    public String getFlgMercadoriaFragil() {
        return flgMercadoriaFragil;
    }

    public void setFlgMercadoriaFragil(String flgMercadoriaFragil) {
        this.flgMercadoriaFragil = flgMercadoriaFragil;
    }

    public Date getDataEntrada() {
        return dataEntrada;
    }

    public void setDataEntrada(Date dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    public Date getDataRetirada() {
        return dataRetirada;
    }

    public void setDataRetirada(Date dataRetirada) {
        this.dataRetirada = dataRetirada;
    }

    public Apartamento getApartamento() {
        return apartamento;
    }

    public void setApartamento(Apartamento apartamento) {
        this.apartamento = apartamento;
    }
}
