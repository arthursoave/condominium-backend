package br.com.soave.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonRootName;

import javax.persistence.*;

/**
 * Model/dataBase model for apartament unit
 *
 * @author soave
 * @since 10/2018
 */

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
@JsonRootName(value = "apartamento")
public class Apartamento {

    @Id
    @GeneratedValue(strategy= GenerationType.SEQUENCE, generator = "apartamento_Sequence")
    @SequenceGenerator(name = "apartamento_Sequence", sequenceName = "APART_SEQ")
    private Long codigoApartamento;

    @Column(name = "NUMERO_APARTAMENTO")
    private int numeroApartamento;

    @Column(name = "TELEFONE_CONTATO")
    private Long telefoneContato;

    @Column(name = "EMAIL_CONTATO")
    private String emailContato;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO_BLOCO", nullable = false)
    @JsonManagedReference
    private Bloco bloco;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CODIGO_CONDOMINIO", nullable = false)
    @JsonBackReference
    private Condominio condominio;

    public Long getCodigoApartamento() {
        return codigoApartamento;
    }

    public void setCodigoApartamento(Long codigoApartamento) {
        this.codigoApartamento = codigoApartamento;
    }

    public int getNumeroApartamento() {
        return numeroApartamento;
    }

    public void setNumeroApartamento(int numeroApartamento) {
        this.numeroApartamento = numeroApartamento;
    }

    public Long getTelefoneContato() {
        return telefoneContato;
    }

    public void setTelefoneContato(Long telefoneContato) {
        this.telefoneContato = telefoneContato;
    }

    public String getEmailContato() {
        return emailContato;
    }

    public void setEmailContato(String emailContato) {
        this.emailContato = emailContato;
    }

    public Bloco getBloco() {
        return bloco;
    }

    public void setBloco(Bloco bloco) {
        this.bloco = bloco;
    }

    public Condominio getCondominio() {
        return condominio;
    }

    public void setCondominio(Condominio condominio) {
        this.condominio = condominio;
    }
}
