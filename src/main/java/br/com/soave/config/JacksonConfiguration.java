package br.com.soave.config;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

/**
 * Overwrite jackson default configuration to get a root element in json objects
 *
 * @author atrpaula
 * @since 10/2018
 */

@Configuration
public class JacksonConfiguration {

    @Bean
    public ObjectMapper objectMapper(){

        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(DeserializationFeature.UNWRAP_ROOT_VALUE);
        //objectMapper.disable(SerializationFeature.WRAP_ROOT_VALUE);
        return objectMapper;
    }
}
